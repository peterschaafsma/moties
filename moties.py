#! /home/jetze/python2/bin/python
# -*- coding: utf-8 -*-

import elasticsearch
import sys

client = elasticsearch.Elasticsearch()


def recreate(mode):
    body_motions = {
        "mappings": {
            "properties": {
                "authors": {
                    "fields": {
                        "keyword": {
                            "ignore_above": 256,
                            "type": "keyword"
                        }
                    },
                    "type": "text"
                },
                "date": {
                    "type": "date"
                },
                "html": {
                    "index": False,
                    "type": "text"
                },
                "passed": {
                    "type": "boolean"
                },
                "text": {
                    "fields": {
                        "keyword": {
                            "ignore_above": 256,
                            "type": "keyword"
                        }
                    },
                    "type": "text"
                },
                "title": {
                    "fields": {
                        "keyword": {
                            "ignore_above": 256,
                            "type": "keyword"
                        }
                    },
                    "type": "text"
                },
                "type": {
                    "type": "keyword"
                },
                "yays": {
                    "type": "keyword"
                },
                "nays": {
                    "type": "keyword"
                },
                "parties": {
                    "type": "keyword"
                },
                "source": {
                    "type": "keyword"
                },
            }
        }
    }

    body_votes = {
        "mappings": {
            "properties": {
                "date": {
                    "type": "date"
                },
                "team_name": {
                    "type": "keyword"
                },
                "pro": {
                    "type": "keyword"
                },
                "con": {
                    "type": "keyword"
                },
                "id": {
                    "type": "keyword"
                }
            }
        }
    }

    client = elasticsearch.Elasticsearch()

    if mode == u'motions':
        try:
            client.indices.delete(index=u'motions')
        except:
            pass
        client.indices.create(index=u'motions', body=body_motions)

    if mode == u'votes':
        try:
            client.indices.delete(index=u'votes')
        except:
            pass
        client.indices.create(index=u'votes', body=body_votes)


if sys.argv[1] == u'init_motions':
    recreate("motions")

if sys.argv[1] == u'init_votes':
    recreate("votes")