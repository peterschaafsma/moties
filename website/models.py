from django.db import models

# Create your models here.
class WorkUnit(models.Model):
    day = models.DateField()
    status = models.CharField(max_length=16, default='todo')
    error_count = models.IntegerField(default=0)