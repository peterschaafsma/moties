from django.core.management.base import BaseCommand


import urllib
import bs4
import sys
import os
import elasticsearch
import re

from website.models import WorkUnit

client = elasticsearch.Elasticsearch()
context = {}

def check_or_fetch(url):
    file_path = 'tmp/' + re.sub(r'[^-a-zA-Z0-9_]', '_', url)

    if os.path.exists(file_path):
        return open(file_path).read()

    contents = urllib.request.urlopen(url).read().decode('utf-8')
    open(file_path, 'w').write(contents)

    return contents

def fetch_for_day(day):
    context[u'day'] = day

    print(u'Fetch for day: ', day.strftime(u'%D'))

    url = u'https://www.tweedekamer.nl/kamerstukken/stemmingsuitslagen?Type=Kamerstukken&fld_tk_categorie=Kamerstukken&fld_tk_subcategorie=Stemmingsuitslagen&todate={day}&fromdate={day}'
    url = url.format(day=day.strftime(u'%d-%m-%Y'))

    soup = bs4.BeautifulSoup(check_or_fetch(url))

    for li in soup.findAll(u'li', class_=u'card__content__list__item'):
        path = li.findChildren(u'a')[0].get(u'href')

        url = u'https://www.tweedekamer.nl' + path

        fetch_collection(url)

        # break

    del context[u'day']

def fetch_collection(url):
    print(u'Fetch collection: ', url)
    soup = bs4.BeautifulSoup(check_or_fetch(url))

    for a in soup.findAll(u'a', class_=u'card'):
        path = a.get(u'href')

        url = u'https://www.tweedekamer.nl' + path

        if path.startswith(u'/kamerstukken/wetsvoorstellen'):
            fetch_wetsvoorstel(url)
        else:
            fetch_motie_or_amendement(url)

        # break

def fetch_wetsvoorstel(url):
    pass

def fetch_motie_or_amendement(url):
    # fetch a specific motie
    # url = u'https://www.tweedekamer.nl/kamerstukken/detail?id=2014Z19517&did=2014D39503'

    print(u'Fetch motie or amendement: ', url)

    soup = bs4.BeautifulSoup(check_or_fetch(url))

    try:
        kamerstuk_path = soup.findAll(u'a', class_=[u'button', u'___rounded', u'___download'])[0].get(u'href')
        kamerstuk_url = u'https://www.tweedekamer.nl' + kamerstuk_path

        id = re.findall(u'(?<=id=)\w+', url)[0]
        did = re.findall(u'(?<=did=)\w+', url)[0]

        file_path = u'tmp/{}_{}'.format(id, did)
        html_path = file_path + u'.html'

        if not os.path.exists(html_path):
            (_, headers) = urllib.request.urlretrieve(kamerstuk_url, file_path)

            if headers.get(u'content-type') == u'application/pdf':
                os.system(u'pdftohtml -noframes {} {}'.format(file_path, html_path))

            elif headers.get(u'content-type') in (u'application/vnd.openxmlformats-officedocument.wordprocessingml.document', ):
                docx_path = file_path + u'.docx'

                os.system(u'cp {} {}'.format(file_path, docx_path))

                os.system(u'convert.py {} {}'.format(docx_path, html_path))

        # get title
        title = soup.find(u'h1', id=u'main-title', class_=u'section__title').get_text(separator=u' ')
        title = re.sub(u'\s+', u' ', title)

        section = soup.find(u'h2', string=u'Ondertekenaars')
        if not section:
            section = soup.find(u'h2', string=u'Indieners')

        # get authors and parties
        authors = []
        parties = []

        party_map = {
            u'partij_voor_de_dieren': u'pvdd',
            u'partij_voor_de_vrijheid': u'pvv',
            u'groep kuzu/\xf6zt\xfcrk': u'groep kuzu/ozturk',
        }

        for a in section.parent.findAll(u'a', class_=u''):
            if a.get(u'href').startswith(u'/kamerleden_en_commissies/alle_kamerleden/'):
                authors.append(a.text)
            elif a.get(u'href').startswith(u'/kamerleden_en_commissies/fracties/'):
                party = a.get(u'href').split(u'/')[-1]
                parties.append(party_map.get(party, party))

        section = soup.find(u'div', class_=u'vote-result-tables')
        if not section:
            print('Not voted')

            return

        yays, nays, mays = [], [], []
        for wrapper in section.findAll(u'div', class_=u'vote-result-table-wrapper')[:3]:
            if wrapper.find(u'th').text.strip() == u'Voor':
                ays = yays
            if wrapper.find(u'th').text.strip() == u'Tegen':
                ays = nays
            if wrapper.find(u'th').text.strip() == u'Afwezig':
                ays = mays

            td = [td.text.strip().lower() for td in wrapper.findAll(u'td')]
            try:
                ays.extend(party_map.get(c, c) for a, b in zip(td[::2], map(int, td[1::2])) for c in [a] * b)
            except ValueError:
                ays.extend(party_map.get(c, c) for c in [re.findall(u'\([^\)]+\)$', ay)[-1][1:-1] for ay in td])

        # get passed
        passed = len(yays) > 75

        html = open(html_path).read()

        # get text
        text = bs4.BeautifulSoup(html).find(u'body').get_text(separator=u' ')

        # get type
        caption = soup.find(u'div', class_=u'section__caption').getText(separator=u' ').lower()
        if caption.find(u'motie') >= 0:
            type = u'motie'
        elif caption.find(u'brief') >= 0:
            type = u'brief'
        elif caption.find(u'amendement') >= 0:
            type = u'amendement'
        elif caption.find(u'motie') >= 0:
            type = u'motie'
        else:
            type = u''

        body = {
            u'title': title,
            u'authors': authors,
            u'parties': parties,
            u'date': context[u'day'],
            u'yays': yays,
            u'nays': nays,
            u'mays': mays,
            u'passed': passed,
            u'type': type,
            u'html': html,
            u'text': text,
            u'source': url
        }

        client.index(index=u'motions', id=u'{}_{}'.format(id, did), body=body)

    except:
        print("ERROR FOR AMENDEMENT", url)

        raise


def fetch():
    while True:
        work_unit = WorkUnit.objects.filter(status='todo').order_by('-day').first()

        if not work_unit:
            print('ALL DONE')
            break

        try:

            fetch_for_day(work_unit.day)

            work_unit.status = 'done'
            work_unit.save()

        except Exception as e:
            work_unit.error_count += 1
            work_unit.save()


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        fetch()