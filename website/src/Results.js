import React from "react";
import { Container, Col, Row, Table, Form } from "react-bootstrap";
import Header from "./Header.js";
import { getCSRFToken } from "./utils.js";
import { DropdownWrapper, SearchControl, DateDropdown } from "./controls.js";

/*


*/

class TopicItem extends React.Component {
    render() {
//        console.log(this.props.data.pro);

        const pro = this.props.data.pro.length;
        const con = this.props.data.con.length;
        const proPercentage = 100.0 * pro / (pro + con);
        const conPercentage = 100.0 * con / (pro + con);

        let proLabel = null;
        let conLabel = null;

        let filters = "";
        if (this.props.filters.teamName) {
            filters += `&team_name=${this.props.filters.teamName}`;
        }

        console.log("filters", this.props.filters, filters);

        if (pro > 0) {
            const idList = this.props.data.pro.map(motion => motion.id).join(',');
            const url = `http://${window.location.host}/list?ids=${idList}${filters}`;

            const label = `Voor: ${pro} - ${proPercentage.toFixed(2)}%`

            proLabel = (
                <a href={url} target="blank">{label}</a>
            );
        }

        if (con > 0) {
            const idList = this.props.data.con.map(motion => motion.id).join(',');
            const url = `http://${window.location.host}/list?ids=${idList}${filters}`;

            const label = `Tegen: ${con} - ${conPercentage.toFixed(2)}%`

            conLabel = (
                <a href={url} target="blank">{label}</a>
            );
        }

        return (
            <tr>
                <td>
                    <span>{this.props.topic}</span>
                </td>
                <td className="vote-bar">
                    <div className="vote-bar">
                        <div style={{width: `${proPercentage}%`}}>
                            <div style={{width: "100%"}} className="vote-bar-item vote-bar-pro"/>
                            {proLabel}
                        </div>
                        <div style={{width: `${conPercentage}%`}}>
                            <div style={{width: "100%"}} className="vote-bar-item vote-bar-con"/>
                            {conLabel}
                        </div>
                    </div>
                </td>
            </tr>
        );
    }
}

class Topics extends React.Component {
    render() {
        if (this.props.data == null) {
            return null;
        }

        const topicItems = Object.keys(this.props.data).map(key => <TopicItem key={key} topic={key} filters={this.props.filters} data={this.props.data[key]}/>)

        return (
            <Table>
                <thead>
                    <tr>
                        <th>Onderwerp</th>
                        <th>Stemgedrag</th>
                    </tr>
                </thead>
                <tbody>
                    {topicItems}
                </tbody>
            </Table>
        );
    }
}

export default class Results extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
            parties: [],
            party: null,
            teamName: "",
            date: null,
        }

        this.onSelect = this.onSelect.bind(this);
        this.filters = this.filters.bind(this);
    }

    componentDidMount() {
        fetch(`http://${window.location.host}/api/parties`)
        .then(response => response.json())
        .then(json => {
            this.setState({parties: json, party: 'vvd'});
        })
    }

    componentDidUpdate(prevProps, prevState) {
        let fetchNewData = false;
        if (this.state.party && prevState.party != this.state.party) {
            fetchNewData = true;
        }
        if (prevState.teamName != this.state.teamName) {
            fetchNewData = true;
        }
        if (prevState.date != this.state.date) {
            fetchNewData = true;
        }

        if (fetchNewData) {
            fetch(`http://${window.location.host}/api/analyze`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': getCSRFToken(),
                },
                body: JSON.stringify(this.filters())
            })
            .then(response => response.json())
            .then(json => {
                this.setState({data: json});
            });
        }
    }

    filters() {
        return {
            party: this.state.party,
            teamName: this.state.teamName,
            date: this.state.date,
        };
    }

    onSelect(selected) {
//        console.log("value", selected);

        if (this.state.party == selected) {
            return;
        }

        this.setState({party: selected});
    }

    render() {
//        console.log("results render", this.state.data);
        if (this.state.data == null) {
            return null;
        }

        const parties = this.state.parties.map(party => { return { key: party.toLowerCase(), value: party }});

        return (
            <Container>
                <Row>
                    <Col>
                        <Header home labels/>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <div>
                            <div className="inline-flex">
                                <span>Partij</span>
                                <DropdownWrapper title={this.state.party || "Partij"} options={parties} onSelect={this.onSelect}/>
                            </div>
                            <DateDropdown value={this.state.date} onChange={value => this.setState({date: value})}/>
                            <SearchControl label="Team" value={this.state.teamName} onChange={value => this.setState({teamName: value})}/>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Topics filters={this.filters()} data={this.state.data}/>
                    </Col>
                </Row>
            </Container>
        );
    }
}