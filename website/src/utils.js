export function getCSRFToken() {
    const cookieValue = document.getElementsByName('csrfmiddlewaretoken')[0].value;

    return cookieValue;
}

