import React from "react";
import { Row, Col, Dropdown, DropdownButton, Button, Form } from "react-bootstrap";


export class DropdownWrapper extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const menuItems = this.props.options.map(item => <Dropdown.Item key={item.key} eventKey={item.key}>{item.value}</Dropdown.Item>);

        return (
            <DropdownButton variant="outline-dark" title={this.props.title} onSelect={this.props.onSelect}>
                {menuItems}
            </DropdownButton >
        );
    }
}

export class Motion extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            expanded: {}
        };

        this.onToggle = this.onToggle.bind(this);
    }

    onToggle(topic) {
        let expanded = {...this.state.expanded};
        expanded[topic] = expanded[topic] ? 0 : 1;

        this.setState({expanded});
    }

    render() {
        console.log("motion render", this.props.motion, this.state);

        if (this.props.motion == null) {
            return null;
        }

        const header = `(${this.props.motion.total} moties voldoen aan de filters)`

        const makeButton = topic => {
            console.log("makeButton", topic, this.state);

            let teamsSection = null;
            if (this.state.expanded[topic[0]]) {
                console.log("EXPANDED", topic[1]);
                const teams = topic[1].map(team => {return <div key={team}>{team}</div>});

                teamsSection = (
                    <div style={{marginLeft: "6px"}}>
                        <strong className="centered">Teams:</strong>
                        {teams}
                    </div>
                );
            }

            return (
                <div className="inline-flex" key={topic[0]}>
                    <Button className="btn-vote btn-light" size="sm" variant="outline-secondary" onClick={() => this.onToggle(topic[0])}>
                        {`${topic[0]} (${topic[1].length})`}
                    </Button>
                    {teamsSection}
                </div>
            );
        }


        let votes = null;
        if (this.props.motion.topics != null) {
            const proTopics = this.props.motion.topics.pro.map(makeButton);
            const conTopics = this.props.motion.topics.con.map(makeButton);

            votes = (
                <Row>
                    <Col md={6}>
                        <div>
                            <h4>Voor:</h4>
                            {proTopics}
                        </div>
                    </Col>
                    <Col md={6}>
                        <div>
                            <h4>Tegen:</h4>
                            {conTopics}
                        </div>
                    </Col>
                </Row>
            );
        }

        if (this.props.motion.total == null) {
            return (
                <div style={{marginBottom: "40px"}}>
                    <h1 className="centered">Kamerstuk</h1>
                    <div dangerouslySetInnerHTML={{__html: this.props.motion.html}}/>
                    {votes}
                </div>
            );
        }
        else {
            if (this.props.motion.total == 0) {
                return (
                    <div>
                        <span>{header}</span>
                    </div>
                );
            }
            else {
                return (
                    <div>
                        <span>{header}</span>
                        <div dangerouslySetInnerHTML={{__html: this.props.motion.html}}/>
                        <div>
                            <a target="blank" href={this.props.motion.source}>Origineel</a>
                            &nbsp;
                            <a target="blank" href={`http://${window.location.host}/list?ids=${this.props.motion.id}`}>Bekijk</a>
                        </div>
                    </div>
                );
            }
        }
    }
}

export class SearchControl extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: props.value
        }

        this.onKeyPress = this.onKeyPress.bind(this);
    }

    onKeyPress(event) {
        if (event.key == "Enter") {
            event.preventDefault();

            this.props.onChange(this.state.value);
        }
    }

    render() {
        return (
            <div className="inline-flex">
                <span>{this.props.label}</span>
                <Form.Control value={this.state.value} onKeyPress={this.onKeyPress} onChange={event => this.setState({value: event.target.value})}></Form.Control>
                <Button variant="outline-light" onClick={() => this.props.onChange(this.state.value)}>🔍</Button>
            </div>
        );
    }
}

export class DateDropdown extends React.Component {
    constructor(props) {
        super(props);

        this.options = [
            { key: null, value: "Alle"},
            { key: 2019, value: "Sinds 2019"},
            { key: 2016, value: "Sinds 2016"},
        ];

        this.optionMap = {};
        this.options.forEach(option => this.optionMap[option.key] = option.value);
    }

    render() {
//        console.log("datedropdown render", this.props);

        const title = this.optionMap[this.props.value];
        const onSelect = selected => this.props.onChange(selected);

        return (
            <div className="inline-flex">
                <span>Tijd</span>
                <DropdownWrapper
                    title={title}
                    options={this.options}
                    onSelect={onSelect}
                />
            </div>
        )
    }
}
