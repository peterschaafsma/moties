import React from "react";
import { Container, Col, Row } from "react-bootstrap";
import { getCSRFToken } from "./utils.js";
import Header from "./Header.js";
import queryString from "query-string";
import { Motion } from "./controls.js";


export default class List extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            motions: [],
        }
    }

    componentDidMount() {
        const values = queryString.parse(this.props.location.search);

        const ids = values.ids.split(',');

        fetch(`http://${window.location.host}/api/list`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "X-CSRFToken": getCSRFToken(),
            },
            body: JSON.stringify({
                ids: ids,
                team_name: this.state.teamName,
            })
        })
        .then(response => response.json())
        .then(json => {
            this.setState({motions: json});
        });
    }

    render() {
        console.log("list render", this.state);

        const header = (
            <Row>
                <Col>
                    <Header home results labels/>
                </Col>
            </Row>
        );

        const motions = this.state.motions.map(motion => (
            <Row key={motion.id}>
                <Col>
                    <Motion motion={motion}/>
                </Col>
            </Row>
        ));

        return (
            <Container>
                {motions}
            </Container>
        );
    }
}