import React from "react";
import { Button, Container, Col, Row, Form } from "react-bootstrap";
import { Typeahead } from "react-bootstrap-typeahead";
import { getCSRFToken } from "./utils.js";
import Header from "./Header.js";
import { DropdownWrapper, Motion, SearchControl, DateDropdown } from "./controls.js";


class OptionInput extends React.Component {
    render() {
//        console.log("optioninput render", this.props.selected, this.props.options);

        const label = <h4>Onderwerpen</h4>;

        const options = this.props.options.map(option => option[0]);

//        console.log("options", options);
        const typeahead = (
            <Typeahead
                onChange={selected => this.props.onChange(selected)}
                options={options}
                multiple={true}
                allowNew={true}
                clearButton={true}
                placeholder="Selecteer onderwerpen..."
                selected={this.props.selected}
            />
        );

        return (
            <div style={{...(this.props.style || {})}} onClick={this.props.onFocus}>
                {label}
                {typeahead}
            </div>
        );
    }
}

class OptionButtons extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            collapsed: true
        }

        this.onToggleCollapsed = this.onToggleCollapsed.bind(this);

        this.collapsedCount = 20;
    }

    onToggleCollapsed() {
        this.setState({collapsed: !this.state.collapsed});
    }

    render() {
//        console.log("optionbuttons render", this.props.options);

        let options;
        if (this.state.collapsed) {
            options = this.props.options.slice(0, this.collapsedCount);
        }
        else {
            options = this.props.options;
        }

        let selected = {};
        this.props.selected.forEach(option => {
            selected[option] = 1;
        });

        const buttons = options.map(option => {
            let props = {};

            if (selected[option[0]]) {
                props.className="btn btn-spaced";
                props.variant="outline-primary";
            }
            else {
                props.className="btn btn-light btn-spaced";
                props.variant="outline-dark";
            }

            return (
                <Button {...props} key={option[0]} onClick={() => this.props.onClick(option[0])}>
                    {option[0]}
                </Button>
            );
        });

        let expandCollapseButton = null;
        if (this.props.options.length > this.collapsedCount) {
            let buttonLabel;
            if (this.state.collapsed) {
                buttonLabel = "...";
            }
            else {
                buttonLabel = "←";
            }

            expandCollapseButton = <Button className="btn btn-light btn-spaced" key="EXPANDCOLLAPSE" onClick={this.onToggleCollapsed}>{buttonLabel}</Button>
        }

        return (
            <div className="btn-collection">
                {buttons}
                {expandCollapseButton}
            </div>
        );
    }
}

class Buttons extends React.Component {
    render() {
        const buttons = this.props.options.map(option => {
            return (
                <Button className="btn btn-spaced" variant="outline-danger" key={option} onClick={() => this.props.onClick(option)}>
                    {option}
                </Button>
            );
        });

        return (
            <div className="btn-collection">
                {buttons}
            </div>
        );
    }
}


class PartyDropdown extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            parties: []
        };
    }

    componentDidMount() {
        fetch(`http://${window.location.host}/api/parties`)
        .then(response => response.json())
        .then(json => this.setState({parties: json}));
    }

    render() {
//        console.log("datedropdown render", this.props);

        const title = this.props.value || "Alle";
        let options = this.state.parties.map(party => { return { key: party, value: party }; });
        options.unshift({key: null, value: "Alle"})
        const onSelect = selected => this.props.onChange(selected);

        return (
            <div className="inline-flex">
                <span>Fractie</span>
                <DropdownWrapper
                    multiple
                    title={title}
                    options={options}
                    onSelect={onSelect}
                />
            </div>
        )
    }
}


class Filters extends React.Component {
    render() {
//        console.log("filters render", this.props.data);

        return (
            <div className="block">
                {this.props.date && <DateDropdown value={this.props.data.date} onChange={value => this.props.onChangeFilter("date", value)}/>}
                {this.props.party && <PartyDropdown value={this.props.data.party} onChange={value => this.props.onChangeFilter("party", value)}/>}
                {this.props.search && <SearchControl label="Zoektermen" value={this.props.data.search} onChange={value => this.props.onChangeFilter("search", value)}/>}
            </div>
        );
    }
}

export default class Labels extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            pro: [],
            con: [],
            options: [],
            autocomplete: [],
            selected: [],
            motion: null,
            teamName: "",
            filters: {
                date: null,
                search: "",
                party: null,
            }
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onToggleOption = this.onToggleOption.bind(this);
        this.onProCon = this.onProCon.bind(this);
        this.updateSelected = this.updateSelected.bind(this);
        this.reloadMotion = this.reloadMotion.bind(this);
        this.resetLabels = this.resetLabels.bind(this);
        this.onChangeFilter = this.onChangeFilter.bind(this);
    }

    componentDidMount() {
        fetch(`http://${window.location.host}/api/labels`)
        .then(response => response.json())
        .then(json => {
            this.setState({autocomplete: json})
        });

        this.reloadMotion();
    }

    componentDidUpdate(prevProps, prevState) {
        let reload = false;
        if (!reload && prevState.filters.date != this.state.filters.date) {
            reload = true;
        }
        if (!reload && prevState.filters.party != this.state.filters.party) {
            reload = true;
        }
        if (!reload && prevState.filters.search != this.state.filters.search) {
            reload = true;
        }

        if (reload) {
            this.reloadMotion();
        }
    }

    reloadMotion() {
        fetch(`http://${window.location.host}/api/motion`, {
            method: "POST",
            headers: {
                "Content-Type": "application-json",
                "X-CSRFToken": getCSRFToken()
            },
            body: JSON.stringify({
                filters: this.state.filters
            })
        })
        .then(response => response.json())
        .then(json => {
            this.setState({motion: json});

            fetch(`http://${window.location.host}/api/labels?id=${json.id}`)
            .then(response => response.json())
            .then(json => {
                this.setState({options: json})
            });
        });

        fetch(`http://${window.location.host}/api/profile`)
        .then(response => response.json())
        .then(json => {
//            console.log("profile", json);

            this.setState({teamName: json.teamName});
        });
    }

    resetLabels() {
        this.setState({selected: [], pro: [], con: []});
    }

    onChange(mode, value) {
        if (mode == "selected") {
            const autocomplete = [...this.state.autocomplete];

            let selected = [];
            value.forEach(item => {
                if (item.customOption) {
                    const option = item.label.toLowerCase();

                    selected.push(option);
                    autocomplete.push([option, 1]);
                }
                else {
                    selected.push(item);
                }
            });

            this.setState({autocomplete});

            this.updateSelected(selected);
        }
        else if (mode == "team") {
            this.setState({teamName: value});
        }
    }

    updateSelected(selected) {
//        console.log("updateSelected", selected, this.state);

        /*
        filter pro and con by current selected
        add all options not in pro or con but in selected to pro
        */

        let selectedMap = {};
        selected.forEach(option => {selectedMap[option] = 1});

        let pro = this.state.pro.filter(value => selectedMap[value]);
        let con = this.state.con.filter(value => selectedMap[value]);

        pro.concat(con).forEach(option => {
            delete selectedMap[option];
        });

        pro.splice(pro.length, 0, ...Object.keys(selectedMap));

        this.setState({pro: pro, con: con, selected: selected});
    }

    onToggleOption(key) {
        let selected = [...this.state.selected];

        const index = selected.indexOf(key);

        if (index >= 0) {
            selected.splice(index, 1);
        }
        else {
            selected.push(key);
        }

        this.updateSelected(selected);
    }

    onProCon(field, key) {
//        console.log("procon", field, key, this.state.pro, this.state.con);

        let pro = [...this.state.pro];
        let con = [...this.state.con];

        if (field == "pro") {
            pro.splice(pro.indexOf(key), 1);
            con.push(key);
        }
        else {
            con.splice(con.indexOf(key), 1);
            pro.push(key);
        }

//        console.log("procon", pro, con);

        this.setState({pro, con});
    }

    onSubmit() {
//        console.log("motion", this.state.motion);
        fetch(`http://${window.location.host}/api/labels`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                'X-CSRFToken': getCSRFToken(),
            },
            body: JSON.stringify({
                pro: this.state.pro,
                con: this.state.con,
                id: this.state.motion.id,
            })})
        .then(response => response.json())
        .then(json => {
//            console.log("submit response", json);

            this.reloadMotion();
            this.resetLabels();
        });
    }

    onChangeFilter(field, value) {
        let filters = {...this.state.filters}
        filters[field] = value;

//        console.log("labels onchangefilter", filters);

        this.setState({filters});
    }

    render() {
//        console.log("render labels", this.state);
        return (
            <Container>
                <Row>
                    <Col>
                        <Header home results/>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <h1 className="centered">{`Team ${this.state.teamName}`}</h1>
                    </Col>
                </Row>
                <Row>
                    <Col md={6}>
                        <Row>
                            <Col>
                                <Filters date search author party data={this.state.filters} onChangeFilter={this.onChangeFilter}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Motion motion={this.state.motion}/>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={6}>
                        <OptionInput options={this.state.autocomplete} selected={this.state.selected} onChange={selected => this.onChange("selected", selected)}/>
                        <OptionButtons options={this.state.options} selected={this.state.selected} onClick={this.onToggleOption}/>
                        <Row>
                            <Col md={6}>
                                <h4>Voor:</h4>
                                <Buttons options={this.state.pro} onClick={key => this.onProCon("pro", key)}/>
                            </Col>
                            <Col md={6}>
                                <h4>Tegen:</h4>
                                <Buttons options={this.state.con} onClick={key => this.onProCon("con", key)}/>
                            </Col>
                        </Row>
                        <Button style={{marginTop: "20px"}} block onClick={this.onSubmit}>Klaar!</Button>
                    </Col>
                </Row>
            </Container>
        );
    }
}