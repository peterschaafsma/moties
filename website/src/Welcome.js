import React from "react";
import { Button, Container, Col, Row, Form, Alert } from "react-bootstrap";
import { Redirect } from "react-router-dom";
import { getCSRFToken } from "./utils.js";
import Header from "./Header.js";


/*
source
purpose
example
results

*/

export default class Welcome extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            teamName: "",
            redirect: null,
        }

        this.onSubmit = this.onSubmit.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
    }

    onSubmit() {
        fetch(`http://${window.location.host}/api/profile`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                'X-CSRFToken': getCSRFToken(),
            },
            body: JSON.stringify({
                teamName: this.state.teamName
            })})
        .then(response => response.json())
        .then(json => {
            if (json.status_code == 200) {
                this.setState({redirect: json.redirect});
            }
            else if (json.status_code == 400) {
                this.setState({error: json.error});
            }
        });
    }

    onKeyPress(event) {
        if (event.key == "Enter") {
            event.preventDefault();

            return this.onSubmit();
        }
    }

    render() {
        if (this.state.redirect) {
            return (
                <Redirect to={this.state.redirect}/>
            );
        }

        let error = null;
        if (this.state.error) {
            error = (
                <Alert variant="warning">
                    {this.state.error}
                </Alert>
            );
        }

        return (
            <Container>
                <Row>
                    <Col>
                        <Header home results/>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <div className="centered">
                            <h1>Moties</h1>
                            <span>by MediaGraphs</span>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col md={6}>
                        <h4>Het probleem</h4>
                        <p>
                            Is GroenLinks wel zo groen als ze doen voorkomen? Is D66 echt een onderwijs-partij, zoals
                            uit hun programma blijkt? Welke partij komt op voor ondernemers? Welke voor onze ouderen?
                            Zorg? Jongeren? Nederlandse normen en waarden?
                        </p>
                        <p>
                            Politici kunnen wel vanalles roepen. Zeker wanneer er verkiezingen aan komen. Maar
                            voegen ze de daad bij het woord als er in de Kamer gestemd moet worden?
                            En even belangrijk, hebben ze dat consequent gedaan of proberen ze op in aanloop naar verkiezingen
                            het gras voor de voeten van de concurrentie weg te maaien?
                        </p>
                        <h4>Stemmen in de Kamer</h4>
                        <p>
                            De overheid publiceert alle uitslagen van stemmingen in de Tweede Kamer.
                            Alles om een goed overzicht te krijgen van het stemgedrag van
                            de volksvertegenwoordiging.
                        </p>
                        <p>
                            Wat de overheid niet publiceert is waar kamerstukken over gaan, en of deze stukken
                            voor danwel tegen een bepaald onderwerp zijn. Dat maakt het lastig om te bepalen of 10 stemmen
                            <i>voor</i> een motie waar het woord 'milieu' in voorkomt ook 10 stemmen voor het milieu zijn.
                            Voor een motie stemmen betekent dus niet per se ook voor de onderwerpen in die motie stemmen!
                        </p>
                        <h4>Crowdsourcing</h4>
                        <p>
                            Kies een team naam, en ga 'Naar de moties!'. U krijgt nu een willekeurig kamerstuk te zien.
                            Bij 'Onderwerpen' kunt u beginnen met het aangeven waar dit stuk over gaat. Later kunt u
                            onder 'Voor:' en 'Tegen:' de onderwerpen verder uitsorteren. Inzake bijvoorbeeld een motie over
                            leraren-salarissen, zou een onderwerp 'verhoging lerarensalarissen' kunnen zijn, en afhankelijk
                            van de tekst van de motie zelf, wordt dit onderwerp onder 'Voor:' of 'Tegen:' geplaatst. Voorts
                            kan een kamerstuk over meerdere onderwerpen gaan. Sommige voor, sommige tegen.
                        </p>
                        <p>
                            Wanneer u klaar bent klikt u op
                            de knop 'Klaar!' en worden de gegevens voor dit kamerstuk opgeslagen. Het is niet erg als u helemaal niets invult,
                            of wanneer de informatie onvolledig is, of zelfs wanneer deze niet helemaal correct is.
                        </p>
                        <p>
                            Wanneer u een aantal stukken van labels heeft voorzien kunt u naar 'Resultaten' gaan. U ziet nu de
                            resultaten van de inbreng van iedereen, en dus niet van alleen uzelf. En hierin ligt ook de kracht
                            van deze site. Door het koppelen van onderwerpen aan kamerstukken te crowdsourcen, hoeft niemand
                            dat monnikenwerk op zich te nemen, en helpen alle beetjes.
                        </p>
                        <p>
                            Ter verificatie van de getoonde data, kunt u via de cijfers onder de rode en groene balken inzien om welke moties
                            het gaat, en waar deze moties over gaan, en of ze <i>voor</i> danwel <i>tegen</i> dat onderwerp zijn.
                            Wanneer een kamerstuk bijvoorbeeld vaker als <i>tegen</i> milieu aangemerkt wordt dan <i>voor</i>, dan
                            wordt het stuk als geheel <i>tegen</i> milieu beschouwd. En een partij die <i>tegen</i> deze motie heeft gestemd,
                            stemt dus <i>voor</i> milieu.
                        </p>
                    </Col>
                    <Col className="centered" md={6}>
                        {error}
                        <Form>
                            <Form.Label>Kies je team naam</Form.Label>
                            <Form.Control value={this.state.teamName} onChange={event => this.setState({teamName: event.target.value})} onKeyPress={this.onKeyPress}/>
                            <Button block className="btn-bottom" variant="primary" onClick={this.onSubmit}>Naar de moties!</Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
        )
    }
}