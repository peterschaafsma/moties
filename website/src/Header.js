import React from "react";


export default class Header extends React.Component {
    render() {
        return (
            <div className="header">
                {this.props.home && <span className="header-item"><a href="/">Home</a></span>}
                {this.props.results && <span className="header-item"><a href="/results">Uitslagen</a></span>}
                {this.props.labels && <span className="header-item"><a href="/labels">Labelen</a></span>}
            </div>
        );
    }
}