import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route } from "react-router-dom";

import Results from "./Results.js";
import Labels from "./Labels.js";
import Welcome from "./Welcome.js";
import List from "./List.js";

import "./Application.scss";
import "react-bootstrap-typeahead/css/Typeahead.css";


export default class Application extends React.Component {
    render() {
//        console.log("application render");
        return (
            <BrowserRouter>
                <div>
                    <Route exact path="/results/" component={Results}/>
                    <Route exact path="/labels/" component={Labels}/>
                    <Route exact path="/list/" component={List}/>
                    <Route exact path="/" component={Welcome}/>
                </div>
            </BrowserRouter>
        );
    }
}


const wrapper = document.getElementById("application");
wrapper ? ReactDOM.render(<Application/>, wrapper) : null;