from django.urls import include, re_path

from api import views

urlpatterns = [
    re_path(r'motion$', views.motion),
    re_path(r'labels$', views.labels),
    re_path(r'analyze$', views.analyze),
    re_path(r'parties$', views.parties),
    re_path(r'profile$', views.profile),
    re_path(r'list', views.list)
]