import elasticsearch
import elasticsearch.helpers

CLIENT = elasticsearch.Elasticsearch()


def get_motion(filters):
    body = {
        'query': {
            'function_score': {
                'random_score': {}
            }
            # 'ids': {
            #     'values': ['W-Lx1G0BDeGH1MsRa_Jm']
            # }
        },
        'size': 1,
        'track_total_hits': True
    }

    if filters.get('date') or filters.get('search') or filters.get('party'):
        body['query']['function_score']['query'] = { 'bool': { 'must': [] }}


    if filters.get('date'):
        query = {
            'range': {
                'date': {
                    'gte': '{}-01-01'.format(filters.get('date'))
                }
            }
        }

        body['query']['function_score']['query']['bool']['must'].append(query)

    if filters.get('search'):
        query = {
            'multi_match': {
                'query': filters.get('search'),
                'fields': ['text', 'title']
            }
        }

        body['query']['function_score']['query']['bool']['must'].append(query)

    if filters.get('party'):
        query = {
            'term': {
                'parties': filters.get('party')
            }
        }

        body['query']['function_score']['query']['bool']['must'].append(query)

    response = CLIENT.search(index='motions', body=body)

    if response['hits']['total']['value'] == 0:
        return { 'total': 0 }

    motion = response['hits']['hits'][0]['_source']
    motion['id'] = response['hits']['hits'][0]['_id']
    motion['total'] = response['hits']['total']['value']
    body = {
        'query': {
            'term': {
                'id': motion['id']
            }
        }
    }

    response = CLIENT.search(index='votes', body=body)

    votes = [hit['_source'] for hit in response['hits']['hits']]

    motion['votes'] = votes

    return motion

def get_motions(ids, filters):
    body = {
        'query': {
            'ids': {
                'values': ids
            }
        },
        'size': len(ids),
        'track_total_hits': True
    }

    response = CLIENT.search(index='motions', body=body)

    motions = {}
    for hit in response['hits']['hits']:
        motion = hit['_source']
        motion['id'] = hit['_id']
        motion['votes'] = []
        motions[hit['_id']] = motion

    body = {
        'query': {
            'bool': {
                'must': [
                    {
                        'terms': {
                            'id': ids
                        }
                    }
                ]
            }
        }
    }

    if filters.get('team_name'):
        query = {
            'term': {
                'team_name': filters.get('team_name'),
            }
        }

        body['query']['bool']['must'].append(query)

    response = CLIENT.search(index='votes', body=body)

    for hit in response['hits']['hits']:
        vote = hit['_source']

        motions[vote['id']]['votes'].append(vote)

    for motion_id in motions.keys():
        votes = motions[motion_id]['votes']

        topics = {'pro': {}, 'con': {}}

        for vote in votes:
            for side in ['pro', 'con']:
                for topic in vote[side]:
                    if topic not in topics[side]:
                        topics[side][topic] = []
                    topics[side][topic].append(vote['team_name'])

        for side in ['pro', 'con']:
            topics[side] = sorted(topics[side].items(), key=lambda x: (-len(topics[side][x[0]]), x[0]))

        motions[motion_id]['topics'] = topics

    return list(motions.values())

def get_labels(id):
    body = {
        'aggregations': {
            'labels_pro': {
                'terms': {
                    'field': 'pro'
                }
            },
            'labels_con': {
                'terms': {
                    'field': 'con'
                }
            }
        },
        'size': 0
    }

    if id:
        body['query'] = { 'term': { 'id': id }}

    response = CLIENT.search(index='votes', body=body)

    labels = {}
    for bucket in response['aggregations']['labels_pro']['buckets']:
        labels[bucket['key']] = bucket['doc_count']

    for bucket in response['aggregations']['labels_con']['buckets']:
        labels[ bucket['key']] = bucket['doc_count'] + labels.get( bucket['key'], 0)

    return labels

def set_labels(labels):
    if labels['con'] or labels['pro']:
        CLIENT.index(index='votes', body=labels)

def get_parties():
    body = {
        'aggregations': {
            'parties': {
                'terms': {
                    'field': 'nays',
                    'size': 100
                }
            }
        }
    }

    response = CLIENT.search(index='motions', body=body)

    parties = sorted(bucket['key'] for bucket in response['aggregations']['parties']['buckets'])

    return parties

def get_analysis(party, team_name, date):
    # get all motion ids that this party voted in

    motion_data = {}
    for field in ['yays', 'nays']:
        body = {
            'query': {
                'bool': {
                    'must': [
                        {
                            'term': {
                                field: party
                            }
                        }
                    ]
                }
            },
            'size': 10000
        }

        if date:
            body['query']['bool']['must'].append({
                'range': {
                    'date': {
                        'gte': '{}-01-01'.format(date)
                    }
                }
            })

        response = CLIENT.search(index='motions', body=body)

        for hit in response['hits']['hits']:
            motion_data[hit['_id']] = hit['_source']
            motion_data[hit['_id']]['id'] = hit['_id']

    party_votes = {}
    for motion_id, motion in motion_data.items():
        yays = motion['yays'].count(party)
        nays = motion['nays'].count(party)

        party_votes[motion_id] = 1 if yays > nays else 0 if nays > yays else None

    # get all topics in those ids
    body = {
        'query': {
            'bool': {
                'must': [
                    {
                        'terms': {
                            'id': list(motion_data.keys()),
                        }
                    }
                ]
            }
        }
    }

    if team_name:
        body['query']['bool']['must'].append({
            'term': {
                'team_name': team_name
            }
        })

    vote_data = {}
    for doc in elasticsearch.helpers.scan(client=CLIENT, index='votes', query=body):
        vote = doc['_source']

        if vote['id'] not in vote_data:
            vote_data[vote['id']] = []

        vote_data[vote['id']].append(vote)

    motion_topic = {}
    for motion_id in vote_data.keys():
        motion_topic[motion_id] = {}
        for vote in vote_data[motion_id]:
            for topic in vote['pro']:
                if topic not in motion_topic[motion_id]:
                    motion_topic[motion_id][topic] = { 'pro': 0, 'con': 0}
                motion_topic[motion_id][topic]['pro'] += 1
            for topic in vote['con']:
                if topic not in motion_topic[motion_id]:
                    motion_topic[motion_id][topic] = { 'pro': 0, 'con': 0}
                motion_topic[motion_id][topic]['con'] += 1

    topic_motions = {}
    for motion_id in motion_topic:
        for topic in motion_topic[motion_id]:
            if topic not in topic_motions:
                topic_motions[topic] = { 'pro': [], 'con': [] }

            data = motion_topic[motion_id][topic]
            if data['pro'] > data['con']:
                topic_motions[topic]['pro'].append(motion_id)
            elif data['pro'] < data['con']:
                topic_motions[topic]['con'].append(motion_id)

    party_topics = {}
    for topic in topic_motions:
        if topic not in party_topics:
            party_topics[topic] = { 'pro': [], 'con': [] }

        for motion_id in topic_motions[topic]['pro']:
            if party_votes[motion_id]:
                party_topics[topic]['pro'].append(motion_id)
            else:
                party_topics[topic]['con'].append(motion_id)

        for motion_id in topic_motions[topic]['con']:
            if not party_votes[motion_id]:
                party_topics[topic]['pro'].append(motion_id)
            else:
                party_topics[topic]['con'].append(motion_id)

    analysis = {}
    for topic in party_topics:
        analysis[topic] = {
            'pro': [motion_data[motion_id] for motion_id in party_topics[topic]['pro']],
            'con': [motion_data[motion_id] for motion_id in party_topics[topic]['con']],
        }

    return analysis