import json
import re

from django.http import JsonResponse

from api.es import get_motion, get_labels, set_labels, get_analysis, get_parties, get_motions

# filters: motions by date, authors, author party, search

# Create your views here.
def motion(request):
    if request.method == 'POST':
        parameters = json.loads(request.body.decode())

        motion = get_motion(parameters['filters'])

        return JsonResponse(motion)

def labels(request):
    if request.method == 'GET':
        labels = get_labels(request.GET.get('id'))

        label_list = sorted(labels.items(), key=lambda x: -labels[x[0]])

        return JsonResponse(label_list, safe=False)

    if request.method == 'POST':
        team_name = request.session.get('team_name', '').strip()
        if not team_name:
            return JsonResponse({'alerts': [{'type': 'warning', 'message': 'Geen team naam'}]})

        # print("store labels into ES: ", request.body.decode())
        label_data = json.loads(request.body.decode())
        label_data['team_name'] = team_name

        set_labels(label_data)

        return JsonResponse({'alerts': [{'type': 'info', 'message': 'Labels opgeslagen'}]})

def parties(request):
    if request.method == 'GET':
        party_data = get_parties()

        return JsonResponse(party_data, safe=False)

def analyze(request):
    if request.method == 'POST':
        parameters = json.loads(request.body.decode())

        analysis = get_analysis(parameters.get('party'), parameters.get('teamName').strip(), parameters.get('date'))

        return JsonResponse(analysis)

def profile(request):
    if request.method == 'GET':
        return JsonResponse({'teamName': request.session.get('team_name', '')})

    if request.method == 'POST':
        parameters = json.loads(request.body.decode())

        team_name = parameters['teamName']

        if re.match(r'[a-zA-Z0-9 ]+', team_name):
            request.session['team_name'] = team_name

            return JsonResponse({'status_code': 200, 'redirect': '/labels'})

        else:
            return JsonResponse({'status_code': 400, 'error': 'Ongeldige naam voor een team'})

def list(request):
    if request.method == 'POST':
        parameters = json.loads(request.body.decode())

        filters = {
            'team_name': parameters.get('team_name')
        }

        if 'ids' in parameters:
            motions = get_motions(parameters['ids'], filters)
        else:
            motions = []

        return JsonResponse(motions, safe=False)