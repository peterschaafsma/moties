# Generated by Django 2.2.4 on 2019-11-05 19:13

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='WorkUnit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('day', models.DateField()),
                ('status', models.CharField(default='todo', max_length=16)),
                ('error_count', models.IntegerField(default=0)),
            ],
        ),
    ]
